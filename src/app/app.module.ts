import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {MaterialModule} from './material-module';
import {StorageBrowser} from './storage.service';
import {FormsModule} from '@angular/forms';
import {ApiService} from './api.service';
import {HttpModule} from '@angular/http';
import {AppRouteModule} from './app-route.module';
import {FirstPageComponent} from './firstPage/firstPage.component';
import {SecondPageComponent} from './scondPage/secondPage.component';
import {ThirdPageComponent} from './thirdPage/thirdPage.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstPageComponent,
    SecondPageComponent,
    ThirdPageComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    FormsModule,
    HttpModule,
    AppRouteModule
  ],
  providers: [
    StorageBrowser,
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
