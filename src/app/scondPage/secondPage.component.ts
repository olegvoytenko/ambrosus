import {Component, OnInit} from '@angular/core';
import {StorageBrowser} from '../storage.service';
import {ApiService} from '../api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'secondPage',
  templateUrl: './secondPage.component.html',
  styleUrls: ['./secondPage.component.scss']
})
export class SecondPageComponent implements OnInit {
  // public generalInfo = {
  //   generalState: false,
  //   offalState: true,
  //   offalStateAfterIncision: true,
  //   inflammatoryLesion: true,
  //   necrosis: true,
  //   sampleCollection: false,
  //   flesh: 'C',
  //   fattyTissue: '1',
  //   remarks: ''
  // };
  constructor(public storageBrowser: StorageBrowser,
              public router: Router,
              public apiService: ApiService) {
  }

  public ngOnInit(): void {
    console.log(this.apiService.generalInfo)
  }

  public test () {
    console.log(this.apiService.generalInfo)
  }
}
