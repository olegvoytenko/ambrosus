import {Component, OnInit} from '@angular/core';
import {StorageBrowser} from './storage.service';
import {ApiService} from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(public storageBrowser: StorageBrowser,
              public apiService: ApiService) {
  }

  public ngOnInit(): void {

  }
}
