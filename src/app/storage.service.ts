import {Injectable} from '@angular/core';

@Injectable()
export class StorageBrowser {

  public get(key: string): any {
    const data: string = localStorage.getItem(key);
    return this.parse(data);
  }

  public set(key: string, value: any): void {
    localStorage.setItem(
      key,
      typeof value === 'object' ? JSON.stringify(value) : value
    );
  }

  private parse(value: any) {
    try {
      return JSON.parse(value);
    } catch (e) {
      return value;
    }
  }
}
