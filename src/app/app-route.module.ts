import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FirstPageComponent} from './firstPage/firstPage.component';
import {SecondPageComponent} from './scondPage/secondPage.component';
import {ThirdPageComponent} from './thirdPage/thirdPage.component';

const routes: Routes = [
  {path: '', component: FirstPageComponent},
  {path: 'huegin', component: SecondPageComponent},
  {path: 'summary', component: ThirdPageComponent},
  {path: '**', component: FirstPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})

export class AppRouteModule {
}
