import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

@Injectable()

export class ApiService {
  public _apiEndpoint = 'https://network.ambrosus.com/';
  public generalInfo = {
    generalState: false,
    offalState: true,
    offalStateAfterIncision: true,
    inflammatoryLesion: true,
    necrosis: true,
    sampleCollection: false,
    flesh: 'C',
    fattyTissue: '1',
    remarks: ''
  };

  constructor(public http: Http) {
  }

  public extractData(res: Response) {
    try {
      const body = res.json();
      return body || {};
    } catch (e) {
      return {};
    }
  }

  public _headers = new Headers({'Content-Type': 'application/json'});

  public post(url: string, data: any, auth?: boolean): Observable<any> {
    return this.http.post(this._apiEndpoint + url, data, this.getHeaders(auth)).map(this.extractData);
  }

  public get(url: string, auth?: boolean): Observable<any> {
    return this.http.get(this._apiEndpoint + url).map(this.extractData);
  }

  public put(url: string, data: any, auth?: boolean): Observable<any> {
    return this.http.put(this._apiEndpoint + url, data, this.getHeaders(auth)).map(this.extractData);
  }

  public deleteRequest(url: string, auth?: boolean): Observable<any> {
    return this.http.delete(this._apiEndpoint + url, this.getHeaders(auth)).map(this.extractData);
  }

  public upload(url: string, data: any): Observable<any> {
    return this.http.post(this._apiEndpoint + url, data, this.getHeadersForUpload());
  }

  public getHeadersForUpload(): RequestOptions {
    const headers = new Headers();
    return new RequestOptions({headers: headers});
  }

  public getHeaders(auth?: boolean): RequestOptions {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new RequestOptions({headers: headers});
  }

  public getAssetbyId(id): Observable<any> {
    return this.get('assets/' + id);
  }

  public sendData(data): Observable<any> {
    return this.post('assets/b1f73f34bee0dcc6804773fb9176bbc8/events', data);
  }

}
