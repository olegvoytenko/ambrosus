import {Component, OnInit} from '@angular/core';
import {StorageBrowser} from '../storage.service';
import {ApiService} from '../api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'thirdPage',
  templateUrl: './thirdPage.component.html',
  styleUrls: ['./thirdPage.component.scss']
})
export class ThirdPageComponent implements OnInit {
  public showModal: boolean;
  public dataToSave;
  constructor(public storageBrowser: StorageBrowser,
              public router: Router,
              public apiService: ApiService) {
  }

  public ngOnInit(): void {
    this.dataToSave = {
      content: {
      secret: '0xd0340d6da90699fc97f4236ac2ae224dc5a1acc8b09c1e8bceeda04ceb0e2269',
        data: {
        type: 'Health Report',
          event_data: this.apiService.generalInfo,
          subject: 'b1f73f34bee0dcc6804773fb9176bbc8',
          owner: '0x0D518aFB00AE379E63283FfB02Eb649e3e865fD2',
          creator: '0x0D518aFB00AE379E63283FfB02Eb649e3e865fD2',
          created_at: Math.floor(new Date().getTime() / 1000)
      }
    }
    };
    // this.dataToSave.content.data
  }

  public sendData () {
    this.apiService.sendData(this.dataToSave).subscribe((data) => {
      if (data) {
        this.showModal = true;
      }
    })
  }

}
