import {Component, OnInit} from '@angular/core';
import {StorageBrowser} from '../storage.service';
import {ApiService} from '../api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'firstPage',
  templateUrl: './firstPage.component.html',
  styleUrls: ['./firstPage.component.scss']
})
export class FirstPageComponent implements OnInit {
  public transportData = {};
  public carcassData = {};
  public transportOrigin = {};
  public cowData = {};
  public transportEvents = {};
  public farmData = {};
  public carcassDataBirthDate = '';
  public carcassDataWeight = '';
  public farmAddress;
  public cowLastWeight;
  public cowDeath = 'No data';
  public transportDeparture = 'No data';
  public cowLastVetCheck = 'No data';
  public cowFlesh = 'No data';
  public farmVetCheck = 'No data';
  public cowFattyTissue = 'No data';
  public cowBirthDate;
  public cowAddress;

  constructor(public router: Router,
              public apiService: ApiService) {
  }

  public ngOnInit(): void {
    // console.log(this.router.url.split('/'))
    const defaultId = '97634881f3cabd30b4d4e322bf6657ef';
    this.apiService.getAssetbyId(defaultId).subscribe((data) => {
      this.carcassData = data.content.data.asset_data;
      const birthDate = new Date(this.carcassData['Birth'] * 1000);
      this.carcassDataBirthDate = `${birthDate.getDate()}.${birthDate.getMonth() + 1}.${birthDate.getFullYear()}`;
      this.apiService.getAssetbyId(this.carcassData['Content']).subscribe((cowData) => {
        this.cowData = cowData.content.data.asset_data;
        this.cowAddress = this.cowData['Address'].join(' ');
        const cowBirthDate = new Date(this.carcassData['Birth'] * 1000);
        this.cowBirthDate = `${cowBirthDate.getDate()}.${cowBirthDate.getMonth() + 1}.${cowBirthDate.getFullYear()}`;
        this.apiService.getAssetbyId(this.carcassData['Content'] + '/events').subscribe((cowEvents) => {
          for (let i = cowEvents.length - 1; i > -1; i--) {
            if (cowEvents[i].content.data.type === 'Killing') {
              const cowDeathDate: Date = new Date(cowEvents[i].content.data.event_data[1] * 1000);
                this.cowDeath = `${cowDeathDate.getDate()}.${cowDeathDate.getMonth() + 1}.${cowDeathDate.getFullYear()}`;
            }
            if (cowEvents[i].content.data.type === 'Weight') {
              this.cowLastWeight = cowEvents[i].content.data.event_data[0];
            }
            if (cowEvents[i].content.data.type === 'Transport') {
              this.transportEvents = cowEvents[i].content.data.event_data;
              const transportDeparture = new Date (this.transportEvents[0] * 1000);
              if (transportDeparture.toDateString() !== 'Invalid Date') {
                this.transportDeparture = `${transportDeparture.getDate()}.${transportDeparture.getMonth() + 1}.${transportDeparture.getFullYear()}`;
              }
              this.apiService.getAssetbyId('551da8ad77373c2b76f75272f565ede5').subscribe((transportData) => {
                this.transportData = transportData.content.data.asset_data;
              });
              this.apiService.getAssetbyId(this.transportEvents[1]).subscribe((transportOrigin) => {
                this.transportOrigin = transportOrigin.content.data.asset_data;
              });
            }
            if (cowEvents[i].content.data.type === 'Vet check') {
              this.cowLastVetCheck = cowEvents[i].content.data.event_data;
            }
            if (cowEvents[i].content.data.type === 'Flesh') {
              this.cowFlesh = cowEvents[i].content.data.event_data;
            }
            if (cowEvents[i].content.data.type === 'Fatty tissue') {
              this.cowFattyTissue = cowEvents[i].content.data.event_data;
            }
          }
        });
      });
    });
    this.apiService.getAssetbyId('97634881f3cabd30b4d4e322bf6657ef/events').subscribe((data) => {
      for (let i = data.length - 1; i > -1; i--) {
        if (data[i].content.data.type === 'Weight') {
          this.carcassDataWeight = data[i].content.data.event_data[0];
          return;
        }
      }
    });
    this.apiService.getAssetbyId('f731d4d6dcc587e4378cc96272cf9fea').subscribe((farmData) => {
      this.farmData = farmData.content.data.asset_data;
      this.farmAddress = farmData.content.data.asset_data['Address'].join(' ');
      this.apiService.getAssetbyId('f731d4d6dcc587e4378cc96272cf9fea/events').subscribe((farmEvents) => {
        for (let i = farmEvents.length - 1; i > -1; i--) {
          if (farmEvents[i].content.data.type === 'Vet check') {
            this.farmVetCheck = farmEvents[i].content.data.event_data;
          }
        }
      });
    });
  }
}
